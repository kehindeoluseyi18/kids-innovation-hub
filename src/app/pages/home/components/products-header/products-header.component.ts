import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-products-header',
  templateUrl: './products-header.component.html',
  styleUrls: ['./products-header.component.css']
})
export class ProductsHeaderComponent {
  @Output()columnsCountChange = new EventEmitter<number>();
  sort = 'desc';
  itmesShowCount = 12;

  onSortUpdated(newSort: string): void {
    this.sort = newSort
  }

    onItmesUpdated(count: number): void{
      this.itmesShowCount = count;
    }
    onColumnsUpdated(colsNum: number): void{
      this.columnsCountChange.emit(colsNum)
    }

}
